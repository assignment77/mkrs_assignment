# mkrs_assignment

## Assignment
Model 2 - Forecasting Electricity Prices

## Files included
1. lbmp_forecast.py
 - takes a directory path to the csv files from a ZIP file I have received and a date from 2017
 - makes a prediction of LBMP for the specified date and compares it to the true data
 - outputs 2 figures:
    1. autocorrelation plot of LBMP in a 170 hours lag range
    2. 3 plots comparing predicted and true data

2. nyc_lbmp_time.html
 - interactive graph of LBMP ($/MWHr), Marginal Cost Losses ($/MWHr), Marginal Cost Congestion ($/MWHr) and their sliding window averages for N.Y.C. load zone

3. mohawk_valley_time.html
 - interactive graph of LBMP ($/MWHr), Marginal Cost Losses ($/MWHr), Marginal Cost Congestion ($/MWHr) and their sliding window averages for Mohawk Valley load zone
 - data from load zones other than N.Y.C. were not utilized; the graph was generated for data exploration purpose only

4. 01_makers_read_data.ipynb
 - jupyter notebook produced during data preparation

5. 02_makers_eda.ipynb
 - jupyter notebook produced during eda

6. 03_makers_supervised_model.ipynb
 - jupyter notebook produced during model development

7. lgbm_regressor_feature_importance.png
 - graph displaying importance of model features

## Usage
Run the script lbmp_forecast.py

Python package versions:
pandas  1.2.3
numpy 1.19.2
sklearn 0.24.1
xgboost 1.6.2
lightgbm 3.2.1

## The model used, key insights and conclusions
The model is Light Gradient Boosting Machine regressor from package lightgbm (https://lightgbm.readthedocs.io/en/latest/pythonapi/lightgbm.LGBMRegressor.html)

Following models were tested:
 - ElasticNet
 - RandomForestRegressor
 - XGBRegressor
 - LGBMRegressor (selected)

Out of all tested features (see 03_makers_supervised_model.ipynb), only 3 were found to provide significant positive impact on the model performance:
 - day of the month
 - hour of the day
 - difference of LBMP lagged by 24 hours and LBMP lagged by 48 hours (autoregressive feature)

Final hyperparameters were found by grid search method with incorporated cross validation for performance evaluation.
The cross validation was performed via 20-fold time series split (24-hour test period always followed the train period; without shuffling).
Time period used for hyperparameter tuning and evaluation spanned from 2017-05-30 00:00:00 to 2017-07-31 23:00:00.
Mean value of MAE (Mean Absolute Error) for 20-fold cross validation was 4.65 $/MWHr with a standard deviation of 2.89.

Hyperparameters found by grid search and used in the final model:
'learning_rate': 0.03
'max_depth': 10
'n_estimators': 200
'num_leaves': 100
'reg_alpha': 0.2
'reg_lambda': 0

Final remark:
There is a room for significant improvement.

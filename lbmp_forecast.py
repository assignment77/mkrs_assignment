# -*- coding: utf-8 -*-
"""
Makers assignment
Model 2 - Forecasting Electricity Prices
"""
import os
import pandas as pd
import numpy as np
from lightgbm import LGBMRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter

def get_path()->str:
    main_dir = input("Enter the full path to the directory containing sub-directories with csv files (...\\2017_NYISO_LBMPs): ")
    last_str_part = main_dir[-16:]
    if "017_NYISO_LBMPs" not in last_str_part:
        print("wrong path")
    return main_dir

def read_data_from_csv()->pd.DataFrame:
    """concat csv files into one and extract only the data for the N.Y.C. zone
    make new columns / features used for the model"""
    main_dir = get_path()
    
    k=0
    df_list = []
    
    for root, dirnames, filenames in os.walk(main_dir):
        if k > 0:
            for fn in filenames:
                if fn.endswith('.csv'):
                    dfcsv = pd.read_csv(root+'\\'+fn, sep=',', header=0)
                    df_list.append(dfcsv)
                else:
                    pass
        else:
            pass
        k+=1
    df = pd.concat(df_list, axis=0)
    df["Time Stamp"] = pd.to_datetime(df["Time Stamp"], dayfirst=True)
    
    df = df[df["Name"] == "N.Y.C."]
    df.set_index("Time Stamp", inplace=True)
    df.sort_index(inplace=True)
    df = df[["LBMP ($/MWHr)"]]
    
    # make new columns
    df["m_day"] = df.index.day # day of the month
    df["hour"] = df.index.hour # hour of the day
    df["lbmp_shift24_minus_shift48"] = df["LBMP ($/MWHr)"].shift(24) - df["LBMP ($/MWHr)"].shift(48) # delta yesterday-day before yesterday for every hour
    
    df.to_csv(main_dir+'\\'+'nyc_lbmp.csv')
    
    return df  

def get_test_date()->str:
    """takes the test day of 2017 as user input"""
    test_day = input("Enter the date to be predicted and teted against the true data. Format MM-DD, like 08-01 for the 1st of August (default)): ")
    if test_day[:2] == "01" or test_day[:2] == "02" or test_day[:2] == "03" or test_day[:2] == "04":
        print("please try again, with dates from May on")
        test_day = input("Enter the date to be predicted and teted against the true data. Format MM-DD, like 08-01 for the 1st of August (default)): ")
    elif len(test_day) < 5:
        test_date = "2017-08-01"
    else:
        test_date = "2017-"+test_day
    return test_date

def define_train_test_periods()->tuple:
    test_date = get_test_date()
    
    test_start = pd.to_datetime(test_date + " 00:00:00")
    test_end = pd.to_datetime(test_date + " 23:00:00")
    
    train_start = test_start - pd.DateOffset(93)
    train_end = test_end - pd.DateOffset(1)
    
    return (test_start, test_end, train_start, train_end)

def calc_mape(y_test, y_predict)->np.float64:
    """returns Mean Absolute Percentage Error"""
    y_true = y_test.values.ravel()
    return np.mean(np.abs((y_true - y_predict) / y_true)) * 100

def score_1_day(model, X_test, y_test):
    """scores the model on a test day and returns predicted values and scores"""
    y_predict = model.predict(X_test)
    
    rmse = np.round(np.sqrt(mean_squared_error(y_test, y_predict)), 2)
    mae = np.round(mean_absolute_error(y_test, y_predict), 2)
    mape = np.round(calc_mape(y_test, y_predict), 2)
    r2 = np.round(r2_score(y_test, y_predict), 2)

    print("Test:")
    print("RMSE: ", rmse)
    print("MAE:  ", mae)
    print("MAPE: ", mape)
    print("R2:   ", r2)
        
    return y_predict, rmse, mae, mape, r2

def results_plot(model, X_test, df_y_true): 
    """shows 3 plots for basic analysis of the prediction results on the test day"""
    y_predict, rmse, mae, mape, r2 = score_1_day(model, X_test, df_y_true)
    
    fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, figsize=(18,6))
    
    ax1.grid()
    ax2.grid()
    ax3.grid()
    
    line45 = np.linspace(min(df_y_true.values), max(df_y_true.values))
    l=np.linspace(min(y_predict), max(y_predict))
    line0 = [0] * len(l)
    
    ax1.plot(df_y_true.values, y_predict, 'o', markersize=5, label='data')
    ax1.plot(line45, line45, color='red', linestyle='--', lw=3, label='ideal line')
    ax1.set_xlabel("observed values")
    ax1.set_ylabel("predicted values")
    
    ax2.plot(y_predict, df_y_true.values.ravel()-y_predict, 'o', markersize=5, label='data')
    ax2.plot(l, line0, color='red', linestyle='--', lw=3, label='ideal line')
    ax2.set_xlabel("predicted values")
    ax2.set_ylabel("residuals (non-standardized)")
    
    ax3.xaxis.set_major_formatter(DateFormatter('%H:%M'))
    ax3.plot(df_y_true.index, df_y_true, color='g', label='reality')
    ax3.plot(df_y_true.index, y_predict, color='b', label='prediction')
    ax3.set_xlabel("time")
    ax3.set_ylabel("LBMP ($/MWHr)")
    
    ax1.legend()
    ax2.legend()
    ax3.legend()
    
    fig.suptitle('test rmse: {rmse}, mae: {mae}, mape: {mape}, r2: {r2}'.format(rmse=rmse, mae=mae, mape=mape, r2=r2))
    plt.show()

def plot_autocorr(df):
    """plot an autocorrelation plot for nyc lbmp"""
    plt.figure(figsize=(16, 8))
    pd.plotting.autocorrelation_plot(df["LBMP ($/MWHr)"]).set_xlim([0, 170])
    plt.title("Autocorrelation plot for N.Y.C. LBMP, lags in hours")
    plt.show()

# define train and test dataframes
dates_tuple = define_train_test_periods() # test_start, test_end, train_start, train_end
test_start = dates_tuple[0]
test_end = dates_tuple[1]
train_start = dates_tuple[2]
train_end = dates_tuple[3]

df = read_data_from_csv()
df_X = df.drop("LBMP ($/MWHr)", axis=1)
df_y = df[['LBMP ($/MWHr)']]

df_X_train = df_X.loc[train_start:train_end, :]
df_y_train = df_y.loc[train_start:train_end, :]

df_X_test = df_X.loc[test_start:test_end, :]
df_y_test = df_y.loc[test_start:test_end, :]

# autocorrelation plot
plot_autocorr(df)

# initiate the LGBM regressor model, fit and score it    
lgbmr_params = {'learning_rate': 0.03, 
                'max_depth': 10, 
                'n_estimators': 200, 
                'num_leaves': 100, 
                'reg_alpha': 0.2, 
                'reg_lambda': 0}

lgbmr_model = LGBMRegressor(objective='regression', metric='rmse', **lgbmr_params)
lgbmr_model.fit(df_X_train, df_y_train)

results_plot(lgbmr_model, df_X_test, df_y_test)
